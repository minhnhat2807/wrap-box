# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Wrap box is to be a monthly subscription service revolving around sports tape. The end user should be able to signup for our service and select the tape products they wish to be shipped each month to their home.
* Version 1.0

### How do I get set up? ###

##### Prerequisite:
* Install [Nodejs](https://nodejs.org/en/), [Bower](https://bower.io/), [MariaDb](https://mariadb.org/)

##### Technologies and frameworks used:
* Nodejs 7.9
* AngularJs 1.6 - UI: bootstrap
* MariaDb

##### How to run on local (Windows):
* Run **npm install** to download needed packages
* Run **npm start** to run; default: [http://localhost:30001](http://localhost:30001)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
* minhnhat2807@gmail.com

