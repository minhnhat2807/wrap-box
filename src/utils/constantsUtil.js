module.exports = Object.freeze({
        ROLES: {
            ADMIN: 'admin',
            USER: 'user'
        },
        STATUS: {
            ACTIVE: 'active',
            INACTIVE: 'inactive',
            PENDING: 'pending',
            DELETED: 'deleted'
        },

        HTTP_STATUS_CODE: {
            UNAUTHORIZED: {CODE: 401, MESSAGE: 'Unauthorized'},
            CREATED: {CODE: 201, MESSAGE: 'Created'},
            NOTFOUND: {CODE: 404, MESSAGE: 'Not Found'},
            BADREQUEST: {CODE: 400, MESSAGE: 'Bad Request'}
        }
    }
);