const log4js = require('koa-log4')
log4js.configure('./config/log4js.json');
const levels = log4js.levels;

const fileLogger = log4js.getLogger('default');
const logglyLogger = log4js.getLogger('loggly');
const mailgunLogger = log4js.getLogger('mailgun');

const config = require('../utils/configUtil');

function getLoggers(level) {
    let loggers = [fileLogger];

    if (level === levels.ERROR.toString() || level === levels.FATAL.toString() || config.logMethod === 'ALL' || config.logMethod === 'LOGGLY') {
        loggers.push(logglyLogger);
    }

    if (level === levels.ERROR.toString() || level === levels.FATAL.toString() || config.logMethod === 'ALL' || config.logMethod === 'MAILGUN') {
        loggers.push(mailgunLogger);
    }

    return loggers;
}

function writeLog() {
    let level = arguments[0].level;
    let loggers = getLoggers(level);

    delete arguments[0].level;

    for (let index = 0; index < loggers.length; index++) {
        let logInstance = loggers[index];
        if (typeof logInstance[level.toLowerCase()] === 'function') {
            let logMethod = logInstance[level.toLowerCase()];
            logMethod.apply(logInstance, arguments[0]);
        }
    }
}

module.exports = {
    trace: function () {
        arguments.level = levels.TRACE.toString();

        writeLog.call(this, arguments);
    },
    debug: function () {
        arguments.level = levels.DEBUG.toString();

        writeLog.call(this, arguments);
    },
    info: function () {
        arguments.level = levels.INFO.toString();

        writeLog.call(this, arguments);
    },
    warn: function () {
        arguments.level = levels.WARN.toString();

        writeLog.call(this, arguments);
    },
    error: function () {
        arguments.level = levels.ERROR.toString();

        writeLog.call(this, arguments);
    },
    fatal: function () {
        arguments.level = levels.FATAL.toString();

        writeLog.call(this, arguments);
    }
};
