﻿var config = require('config');

module.exports = {
    dbConfig: function () {
        return config.get('dbConfig');
    }(),

    root: function () {
        return config.get('root');
    }(),

    limit: function () {
        var pageSize = 10;
        if (config.has('limit')) {
            pageSize = parseInt(config.get('limit'));
        }

        return pageSize;
    }(),

    jwtSecretKey: function () {
        return config.get('jwtSecretKey');
    }(),

    smtp: function () {
        return config.get('smtp');
    }(),

    logMethod: function () {
        return config.get('logMethod');
    }(),

    stripe: function () {
        return config.get('stripe');
    }()
};