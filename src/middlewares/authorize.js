const jwt = require('jsonwebtoken');
const utils = require('../utils/index');
const accountService = require('../services/accountService');

function authorize(roles) {
    return async function (ctx, next) {
        var errorCode = utils.CONSTANTS.HTTP_STATUS_CODE.UNAUTHORIZED.CODE;
        var errorMessage = utils.CONSTANTS.HTTP_STATUS_CODE.UNAUTHORIZED.MESSAGE;

        roles = roles || [utils.CONSTANTS.ROLES.USER];
        var token = ctx.headers['access-token'];
        if (!token) {
            ctx.throw(errorMessage, errorCode);
        }

        var decodeData = jwt.verify(token, utils.config.jwtSecretKey);
        if (!decodeData || (decodeData.exp * 1000 < Date.now())) {
            console.log('TOKEN');
            ctx.throw(errorMessage, errorCode);
        }

        var user = await accountService.getById(decodeData.id);
        if (!user) {
            console.log('USER');
            ctx.throw(errorMessage, errorCode);
        }

        if (user.role !== utils.CONSTANTS.ROLES.ADMIN && roles.indexOf(user.role) === -1) {
            console.log('ROLE');
            ctx.throw(errorMessage, errorCode);
        }

        ctx.state.user = user;

        await next();
    }
};

module.exports = authorize;