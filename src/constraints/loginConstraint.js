module.exports = {
    email: {
        presence: true
    },
    password: {
        presence: true
    }
};