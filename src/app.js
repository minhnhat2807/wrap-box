const http = require('http');
const mkdirp = require('mkdirp');
const bodyParser = require('koa-bodyparser');
const router = require('koa-router')();
const Koa = require('koa');
const app = new Koa();

mkdirp('./logs');

const dbContext = require('./models/dbContext');
const dataService = require('./services/dataService');
const utils = require('./utils/index');

app.use(bodyParser());

app.on('error', (err, ctx) => {
    utils.logger.error('Server error', err, ctx);
});

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        ctx.status = err.status || 500;
        let message = err.message;

        if (ctx.status === 500) {
            message = 'Oops! Something went wrong. Please try again later';
            ctx.app.emit('error', err, ctx);
        }

        ctx.body = {message: message};
    }
});

require('./routes/index')(router);

app.use(router.routes()).use(router.allowedMethods());

dbContext.sequelize.sync({force: false}).then(function () {
    http.createServer(app.callback()).listen(process.env.PORT || 3001, function () {
        utils.logger.trace('listening on port 3001');

        dataService.init();
    });
});

