const validate = require('validate.js');

const dbContext = require('../models/dbContext');
const utils = require('../utils/index');
const loginConstraint = require('../constraints/loginConstraint');

async function authenticate(model) {
    var errors = validate(model, loginConstraint);
    if (errors) {
        throw {status: utils.CONSTANTS.HTTP_STATUS_CODE.BADREQUEST.CODE, message: errors}
    }

    var email = model.email;
    var password = utils.security.hash(model.password, email);

    return await dbContext.users.findOne({
        where: {email: email, passWord: password},
        attributes: ['id', 'email', 'firstName', 'lastName', 'status']
    });
}

async function getById(id) {
    if (!id || id < 0) {
        return null;
    }

    return await dbContext.users.findOne({
        where: {'id': id},
        attributes: ['id', 'email', 'firstName', 'lastName', 'role']
    });
};

module.exports = {
    authenticate: authenticate,
    getById: getById
};