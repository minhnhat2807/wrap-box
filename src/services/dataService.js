const dbContext = require('../models/dbContext');
const utils = require('../utils/index');

async function createSystemAdmin() {
    await dbContext.users.findOrCreate({
        where: {
            email: 'systemadmin@gmail.com'
        },
        attributes: ['id'],
        defaults: {
            firstName: 'Admin',
            lastName: 'Admin',
            email: 'systemadmin@gmail.com',
            password: utils.security.hash('$admin$wrap$box$', 'systemadmin@gmail.com'),
            status: utils.CONSTANTS.STATUS.ACTIVE,
            isEmailVerified: true,
            role: utils.CONSTANTS.ROLES.ADMIN
        }
    });
}

async function init() {
    await createSystemAdmin();
}

module.exports = {
    init: init
};
