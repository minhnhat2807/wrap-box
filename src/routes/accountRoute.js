const accountController = require('../controllers/accountController');

module.exports = function (router) {
    router.post('/api/account/auth', accountController.auth);
}
