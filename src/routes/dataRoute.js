const authorize = require('../middlewares/authorize');

module.exports = function (router) {
    router.get('/api/data', authorize(), function (ctx) {
        ctx.body = 'Sample API';
    });

    router.post('/api/test/authorize', authorize(), async function (ctx) {
        try {
            ctx.body = ctx.state.user;
        }
        catch (ex) {
            console.log(ex);
        }
    });

    router.post('/api/data/init', async function (ctx) {
        try {
            ctx.body = 'Ok';
        }
        catch (ex) {
            console.log(ex);
        }
    });
};