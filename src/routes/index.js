const send = require('koa-send');

module.exports = function (router) {

    router.get('/', async function (ctx, next) {
        await send(ctx, 'client/main/index.html');
    });

    router.get('/admin', async function (ctx, next) {
        await send(ctx, 'client/admin/index.html');
    });

    require('./dataRoute')(router);
    require('./accountRoute')(router);
};
