const Sequelize = require('sequelize');
const utils = require('../utils/index');
const configUtil = utils.config;

// http://docs.sequelizejs.com/en/1.7.0/docs/usage/
let sequelize = new Sequelize(configUtil.dbConfig.database, configUtil.dbConfig.user, configUtil.dbConfig.password, {
    host: configUtil.dbConfig.host,
    port: configUtil.dbConfig.port,

    dialect: 'mariadb',

    pool: {maxConnections: 5, maxIdleTime: 30},

    // disable logging; default: console.log
    logging: console.log,

    // max concurrent database requests; default: 50
    maxConcurrentQueries: 100
});

sequelize.authenticate().then(function (err) {
    console.log('Connection has been established successfully.');
}).catch(function (err) {
    console.log('Unable to connect to the database:', err);
});

module.exports = {
    Sequelize: Sequelize,
    sequelize: sequelize,
    users: sequelize.import(__dirname + '/user')
};
