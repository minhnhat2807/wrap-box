// http://docs.sequelizejs.com/en/latest/docs/models-definition/
const utils = require('../utils/index');
const CONSTANTS = utils.CONSTANTS;

module.exports = function (sequelize, DataTypes) {
    return sequelize.define("user", {
        firstName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        lastName: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false,
            unique: true
        },
        password: {
            type: DataTypes.STRING(300),
            allowNull: false
        },
        status: {
            type: DataTypes.ENUM,
            values: [CONSTANTS.STATUS.ACTIVE, CONSTANTS.STATUS.INACTIVE, CONSTANTS.STATUS.PENDING, CONSTANTS.STATUS.DELETED],
            defaultValue: 'pending',
            allowNull: false
        },
        phone: {
            type: DataTypes.STRING(30),
            allowNull: true
        },
        role: {
            type: DataTypes.ENUM,
            values: [CONSTANTS.ROLES.ADMIN, CONSTANTS.ROLES.USER],
            defaultValue: 'user',
            allowNull: false
        },
        isEmailVerified: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        verificationToken: {
            type: DataTypes.STRING(300),
            allowNull: true
        },
        verificationTokenExpirationOn: {
            type: DataTypes.DATE,
            allowNull: true
        },
        stripeId: {
            type: DataTypes.STRING(100),
            allowNull: true
        }
    });
};