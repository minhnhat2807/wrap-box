const jwt = require('jsonwebtoken');
const moment = require('moment');

const utils = require('../utils/index');
const accountService = require('../services/accountService');

async function auth(ctx) {
    var model = {
        email: ctx.request.body.email,
        password: ctx.request.body.password
    };

    var user = await accountService.authenticate(model);

    if (!user) {
        ctx.throw(401, 'user name or password is incorrect');
    }

    if (user.status != utils.CONSTANTS.STATUS.ACTIVE) {
        ctx.throw(401, `Account is ${user.status}. Please contact administrator for help`);
    }

    const TOKEN = jwt.sign({id: user.id}, utils.config.jwtSecretKey, {expiresIn: '1d'});

    ctx.body = {
        id: user.id,
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        access_token: TOKEN,
        '.expires': moment().add(1, 'days'),
        '.issued': moment()
    };
}

module.exports = {
    auth: auth
};